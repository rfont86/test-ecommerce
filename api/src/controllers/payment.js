const stripe = require('stripe')(process.env.SECRET_KEY_STRIPE)

const payment = {}

payment.paymentIntent = async (req, res) => {
  console.log(req.body);
  const paymentIntent = await stripe.paymentIntents.create({
    amount: req.body.amount,
    customer: req.body.customer,
    description: req.body.description,
    currency: 'eur',
    payment_method_types: ['card'],
  });

  res.json(paymentIntent)
}

payment.confirm = async (req, res) => {
  const paymentIntent = await stripe.paymentIntents.confirm(
    req.params.id,
    { payment_method: 'pm_card_visa' }
  );

  res.json(paymentIntent)
}

payment.cancel = async (req, res) => {
  console.log(req.params);
  const paymentIntent = await stripe.paymentIntents.confirm(req.params.id,
    { payment_method: 'pm_card_visa' })

  res.json(paymentIntent)
}

module.exports = payment