import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaymentIntentDto } from '../model/payment-intent-dto';

const CABECERA = { headers: new HttpHeaders({'Content-Type': 'application/json'})}


@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  stripeUrl: string = 'http://localhost:4000/stripe/'


  constructor(private httpClient: HttpClient ) { }

  public pagar(paymentIntentDto: PaymentIntentDto): Observable<string> {
    // console.log(paymentIntentDto);
    
    return this.httpClient.post<string>(this.stripeUrl + `paymentintent`, paymentIntentDto, CABECERA)
  }

  public confirmar(id: string): Observable<string> {
    return this.httpClient.post<string>(this.stripeUrl + `confirm/${id}`,{}, CABECERA)
  }

  public cancelar(id: string): Observable<string> {
    return this.httpClient.post<string>(this.stripeUrl + `cancel/${id}`,{}, CABECERA)
  }
}
