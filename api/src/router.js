const { Router } = require('express')
const router = Router()

const { listaArticulos, item, createItem } = require('./controllers/articulo')

const { paymentIntent, confirm, cancel } = require('./controllers/payment')


router.get("/articulo/lista", listaArticulos)

router.get("/articulo/detalle/:id", item)

router.post("/upload", createItem)

router.post("/stripe/paymentintent", paymentIntent)

router.post("/stripe/confirm/:id", confirm)

router.post("/stripe/cancel/:id", cancel)


module.exports = router