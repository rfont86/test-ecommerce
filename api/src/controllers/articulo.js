const Articulo = require('../models/Articulo')

const articuloController = {}

articuloController.listaArticulos = async (req, res) => {
  const lista = await Articulo.find()
  res.json(lista)
}

articuloController.item = async (req, res) => {
  const {id} = req.params
  const result = await Articulo.findById(id)
  res.json(result)
}

articuloController.createItem = async (req, res) => {
  const {nombre, descripcion, precio, imagenURL} = req.body
  const newArticulo = new Articulo({nombre, descripcion, precio, imagenURL})
  await newArticulo.save()
  res.status(200)
}

module.exports = articuloController