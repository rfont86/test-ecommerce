export interface PaymentIntentDto {
  token: string,
  customer: string,
  description: string,
  amount: number,
  currency: string
}
