const mongoose = require('mongoose')

const { ECOMMERCE_MONGODB_HOST, ECOMMERCE_MONGODB_DATABASE} = process.env
const MONGODB_URI = `mongodb://${ECOMMERCE_MONGODB_HOST}/${ECOMMERCE_MONGODB_DATABASE}`

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(db => console.log("db connected"))
  .catch(err => console.log(err))