const express = require('express')
const cors = require('cors')
const server = express()

server.set('port', process.env.PORT || 4000)

server.use(cors())

server.use(express.json())

server.use(require('./router'))

module.exports = server