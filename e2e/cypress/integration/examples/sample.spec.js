describe('My First Test', () => {
  it('Visits the Kitchen Sink', () => {
    cy.visit('http://localhost:4201')

    cy.get('.btn').click()

    cy.url()
      .should('include', '/detalle/5f494f047ae6a00011c16e00')

    cy.get('.form-control').type('Pepe')
    cy.getWithinIframe('[name="cardnumber"]').type('4242424242424242')
    cy.getWithinIframe('[name="exp-date"]').type('1232');
    cy.getWithinIframe('[name="cvc"]').type('987');
    cy.getWithinIframe('[name="postal"]').type('12345');
  })
})