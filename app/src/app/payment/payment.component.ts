import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { StripeService, StripeCardComponent } from 'ngx-stripe';
import {
  StripeCardElementOptions,
  StripeElementsOptions
} from '@stripe/stripe-js';
 
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentService } from './payment.service';
import { PaymentIntentDto } from '../model/payment-intent-dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  @ViewChild(StripeCardComponent, {static: true}) card: StripeCardComponent;

  @Input() precio
  @Input() descripcion
  @Input() nombre

  customer: string

  error: any

  // card: StripeCardComponent
  // cardOptions: StripeCardElementOptions

  elementsOptions: StripeElementsOptions = {
    locale: 'es'
  };

  constructor(
    public modalService: NgbModal,
    public stripeService: StripeService,
    private paymentService: PaymentService,
    private router: Router
  ) { }

  public stripeForm = new FormGroup({
    name: new FormControl('', Validators.required)
  })

  getName(value){
    this.customer = value.target.value
    
  }

  cardOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        fontWeight: '300',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };

  ngOnInit() {
    
  }

  createToken(): void {
    const name = this.stripeForm.get('name').value;
    this.stripeService
      .createToken(this.card.element, { name })
      .subscribe((result) => {
        if (result.token) {
          const paymentIntentDto: PaymentIntentDto = {
            token: result.token.id,
            amount: this.precio,
            customer: this.customer,
            currency: 'EUR',
            description: this.descripcion
          }
          this.paymentService.pagar(paymentIntentDto).subscribe(
            data => {
              this.abrirModal(data['id'], this.nombre, data['description'], data['amount'])
              this.router.navigate(['/'])
            }
          )
          this.error = undefined
        } else if (result.error) {
          // Error creating the token
          this.error = result.error.message 
        }
      });
  }

  abrirModal(id: string, nombre: string, descripcion: string, precio: number){
    const modalRef = this.modalService.open(ModalComponent)
    modalRef.componentInstance.id = id
    modalRef.componentInstance.nombre = nombre
    modalRef.componentInstance.descripcion = descripcion
    modalRef.componentInstance.precio = precio
  }

}
