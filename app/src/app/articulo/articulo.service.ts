import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Articulo } from '../model/articulo'

const CABECERA = { headers: new HttpHeaders({'Content-Type': 'application/json'})}

@Injectable({
  providedIn: 'root'
})

export class ArticuloService {

  articuloUrl: string = 'http://localhost:4000/articulo/'

  constructor(private httpClient: HttpClient ) { }

  public lista(): Observable<Articulo[]> {
    return this.httpClient.get<Articulo[]>(this.articuloUrl + 'lista', CABECERA)
  }

  public detalle(id: number): Observable<Articulo> {
    return this.httpClient.get<Articulo>(this.articuloUrl + `detalle/${id}`, CABECERA)
  }
}
