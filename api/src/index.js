require('dotenv').config()

const api = require('./server')
require('./database')

api.listen(api.get('port'), () => {
  console.log("Server listining on port", api.get('port'));
})