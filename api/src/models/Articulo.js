const { Schema, model} = require('mongoose')

const ArticuloSchema = new Schema({
  nombre: { type: String, required: true },
  descripcion: { type: String, required: true },
  precio: { type: Number, required: true },
  imagenURL: { type: String, required: true },
}, {
  timestamps: true
})

module.exports = model('Articulo', ArticuloSchema)